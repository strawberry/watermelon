# Watermelon

A web client for the Strawberry protocol.

## Using MightyMeld

While we include the configuration files needed for MightyMeld, we do not include it as a dependency. It is entirely optional.

However, if you choose to sacrifice some user freedom to improve the design experience with MightyMeld, you can set up MightyMeld with following commands:

```
npm i -g @mightymeld/runtime
npm link @mightymeld/runtime
```

Then you can simply run `npx mightymeld` to open Watermelon in MightyMeld Studio.
