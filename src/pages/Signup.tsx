import { Center, Heading } from "@chakra-ui/react";

export default function Signup() {
	return (
		<>
			<Center
				style={{
					marginTop: "32px"
				}}
			>
				<Heading
					size={{
						base: "lg",
						md: "2xl"
					}}
				>
					Sign up
				</Heading>
			</Center>
		</>
	);
}
