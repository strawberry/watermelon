import { Button, Center, Heading } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";

export default function Home() {
	const navigate = useNavigate();

	return (
		<>
			<Center
				style={{
					marginTop: "32px"
				}}
			>
				<Heading
					size={{
						base: "2xl",
						sm: "3xl",
						md: "4xl"
					}}
				>
					Watermelon
				</Heading>
			</Center>

			<Center style={{ marginTop: "64px" }}>
				<Button
					mr="8px"
					p={{
						base: "32px",
						sm: "48px"
					}}
					borderRadius="4px"
					fontSize={{
						base: "16px",
						sm: "20px"
					}}
					fontWeight="bold"
					bg="#3b82f6"
					color="#fff"
					_hover={{ bg: "#60a5fa" }}
					onClick={() => {
						navigate("/login");
					}}
				>
					Log in
				</Button>
				<Button
					ml="8px"
					p={{
						base: "32px",
						sm: "48px"
					}}
					borderRadius="4px"
					fontSize={{
						base: "16px",
						sm: "20px"
					}}
					fontWeight="bold"
					bg="#3b82f6"
					color="#fff"
					_hover={{ bg: "#60a5fa" }}
					onClick={() => {
						navigate("/signup");
					}}
				>
					Sign up
				</Button>
			</Center>
		</>
	);
}
