import { Button, Center, Heading, Icon, Text } from "@chakra-ui/react";
import { ChangeEvent, useEffect, useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import Buffer from "../strawberry/buffer";
import Deserializer from "../strawberry/deserializer";
import { deserializeUser } from "../strawberry/user";

export default function Login() {
	const navigate = useNavigate();
	const ref = useRef<HTMLInputElement | null>(null);

	const createUploader = (onFile: any) => {
		return () => {
			if (ref.current === null) {
				return;
			}

			ref.current.onchange = (e: any) => {
				const target = (e as ChangeEvent).target as HTMLInputElement;

				if (target.files === null || target.files.length === 0) {
					console.log(target.files);
					return;
				}

				const file = target.files[0];

				onFile(file);
			};

			ref.current.click();
		};
	};

	const [userObject, setUserObject] = useState<Uint8Array | null>(
		localStorage.getItem("watermelon-user-object") !== null
			? (new (Uint8Array as any)(
					localStorage.getItem("watermelon-user-object")?.split(",")
				) as Uint8Array)
			: null
	);
	const [privateKey, setPrivateKey] = useState<string | null>(
		localStorage.getItem("watermelon-private-key")
	);

	const checkUploads = () => {
		const userObject =
			localStorage.getItem("watermelon-user-object") !== null
				? (new (Uint8Array as any)(
						localStorage.getItem("watermelon-user-object")?.split(",")
					) as Uint8Array)
				: null;
		const privateKey = localStorage.getItem("watermelon-private-key");

		if (userObject !== null && privateKey !== null) {
			navigate("/");
		}
	};

	useEffect(() => {
		setTimeout(checkUploads, 1000);
	}, []);

	if (userObject !== null && privateKey !== null) {
		return (
			<>
				<Center
					style={{
						marginTop: "32px"
					}}
				>
					<Heading size="md">You are already logged in. Redirecting...</Heading>
				</Center>
			</>
		);
	}

	return (
		<>
			<Center
				style={{
					marginTop: "32px"
				}}
			>
				<Heading
					size={{
						base: "lg",
						md: "2xl"
					}}
				>
					Log in
				</Heading>
			</Center>

			<Center style={{ marginTop: "32px" }}>
				<Text>Strawberry does not authenticate with passwords.</Text>
			</Center>

			<Center style={{ marginTop: "8px" }}>
				<Text>You will need to upload your private key and user object.</Text>
			</Center>

			<Center style={{ marginTop: "32px" }}>
				<Button
					mr="8px"
					p={{
						base: "32px",
						sm: "48px"
					}}
					borderRadius="4px"
					fontSize={{
						base: "16px",
						sm: "20px"
					}}
					fontWeight="bold"
					bg={userObject === null ? "#3b82f6" : "#93c5fd"}
					color="#fff"
					_hover={{ bg: userObject === null ? "#60a5fa" : "#93c5fd" }}
					onClick={
						userObject === null
							? createUploader(async (file: File) => {
									const data = new Uint8Array(await file.arrayBuffer());
									const buf = new Buffer(data);
									const d = new Deserializer(buf, false);

									console.log(d.readHeader());
									console.log(deserializeUser(d));

									localStorage.setItem(
										"watermelon-user-object",
										data.join(",")
									);
									checkUploads();
									setUserObject(data);
								})
							: () => {}
					}
					disabled={userObject !== null}
					style={{
						cursor: userObject === null ? "pointer" : "not-allowed"
					}}
				>
					Upload user object
				</Button>
				<Button
					ml="8px"
					p={{
						base: "32px",
						sm: "48px"
					}}
					borderRadius="4px"
					fontSize={{
						base: "16px",
						sm: "20px"
					}}
					fontWeight="bold"
					bg={privateKey === null ? "#3b82f6" : "#93c5fd"}
					color="#fff"
					_hover={{ bg: privateKey === null ? "#60a5fa" : "#93c5fd" }}
					onClick={
						privateKey === null
							? createUploader(async (file: File) => {
									const privateKey = await file.text();

									localStorage.setItem("watermelon-private-key", privateKey);
									checkUploads();
									setPrivateKey(privateKey);
								})
							: () => {}
					}
					disabled={privateKey !== null}
					style={{
						cursor: privateKey === null ? "pointer" : "not-allowed"
					}}
				>
					Upload private key
				</Button>

				<input ref={ref} type="file" style={{ display: "none" }} />
			</Center>
		</>
	);
}
