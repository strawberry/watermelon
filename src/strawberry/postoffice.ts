import Deserializer from "./deserializer";

export default class Mailbox {
	username: string;
	server: string;
	type: string;

	constructor(username: string, server: string, type: string) {
		this.username = username;
		this.server = server;
		this.type = type;
	}
}

export function deserializeMailbox(d: Deserializer): Mailbox {
	return new Mailbox(d.readString(), d.readString(), d.readString());
}
