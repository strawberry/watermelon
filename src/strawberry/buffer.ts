// TypeScript re-implementation of Go's bytes.Buffer

export default class Buffer {
	data: Uint8Array;
	private readIndex: number;

	constructor(data?: Uint8Array) {
		if (typeof data !== "undefined") {
			this.data = data;
		} else {
			this.data = new Uint8Array();
		}

		this.readIndex = 0;
	}

	read(n: number): Uint8Array {
		if (this.readIndex >= this.data.length) {
			throw new Error("EOF");
		}

		const data = this.data.slice(this.readIndex, this.readIndex + n);

		this.readIndex += n;

		return data;
	}

	write(data: Uint8Array) {
		this.data = new Uint8Array(
			// @ts-ignore
			this.data.join(",").split(",").concat(data.join(",").split(","))
		);
	}
}
