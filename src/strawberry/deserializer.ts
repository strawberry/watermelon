export default class Deserializer {
	reader: any;
	data: Uint8Array;
	shouldWriteToDataSlice: boolean;

	constructor(reader: { read: any }, shouldWriteToDataSlice: boolean) {
		this.reader = reader;
		this.data = new Uint8Array();
		this.shouldWriteToDataSlice = shouldWriteToDataSlice;
	}

	readRawBytes(n: number): Uint8Array {
		const data: Uint8Array = this.reader.read(n);

		if (this.shouldWriteToDataSlice) {
			this.data = new Uint8Array(
				// @ts-ignore
				this.data.join(",").split(",").concat(data.join(",").split(","))
			);
		}

		return data;
	}

	readUint32(): number {
		return new DataView(this.readRawBytes(4).buffer).getUint32(0);
	}

	readUint64(): bigint {
		return new DataView(this.readRawBytes(8).buffer).getBigUint64(0);
	}

	readBytearray(): Uint8Array {
		const length = this.readUint32();
		return this.readRawBytes(length);
	}

	readString(): string {
		return new TextDecoder("utf-8").decode(this.readBytearray());
	}

	readHeader(): { protocolVersion: any; objectType: string } {
		const magic = this.readRawBytes(4);

		if (magic.join(",") !== "79,66,74,72") {
			throw new Error(
				`Expected magic bytes [79, 66, 74, 72], got ${magic.join(", ")}`
			);
		}

		const header = {
			protocolVersion: this.readRawBytes(1)[0],
			objectType: this.readString()
		};

		const headerFooterMagic = this.readRawBytes(4);

		if (headerFooterMagic.join(",") !== "79,66,74,83") {
			throw new Error(
				`Expected [79, 66, 74, 83], got ${headerFooterMagic.join(", ")}`
			);
		}

		return header;
	}

	readFooter(): { signature: Uint8Array } {
		const magic = this.readRawBytes(4);

		if (magic.join(",") !== "79,66,74,69") {
			throw new Error(
				`Expected magic bytes [79, 66, 74, 69], got ${magic.join(", ")}`
			);
		}

		return {
			signature: this.readBytearray()
		};
	}
}
