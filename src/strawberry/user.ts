import Deserializer from "./deserializer";
import { deserializeMailbox } from "./postoffice";

export default class User {
	displayName: string;
	description: string;
	created: bigint;
	lastUpdated: bigint;
	mailbox: any;
	signatureKey: any;

	constructor(
		displayName: string,
		description: string,
		created: bigint,
		lastUpdated: bigint,
		mailbox: any,
		signatureKey: any
	) {
		this.displayName = displayName;
		this.description = description;
		this.created = created;
		this.lastUpdated = lastUpdated;
		this.mailbox = mailbox;
		this.signatureKey = signatureKey;
	}
}

export function deserializeUser(d: Deserializer): User {
	return new User(
		d.readString(),
		d.readString(),
		d.readUint64(),
		d.readUint64(),
		deserializeMailbox(d),
		{}
	);
}
