import { RouterProvider, createBrowserRouter } from "react-router-dom";
import { ChakraProvider, extendTheme } from "@chakra-ui/react";
import React, { Suspense } from "react";
import ReactDOM from "react-dom/client";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Signup from "./pages/Signup";
import Deserializer from "./strawberry/deserializer";
import Buffer from "./strawberry/buffer";

(window as any).Deserializer = Deserializer;
(window as any).Buffer = Buffer;

const router = createBrowserRouter([
	{
		path: "/",
		children: [
			{
				index: true,
				element: <Home />
			},
			{
				path: "login",
				element: <Login />
			},
			{
				path: "signup",
				element: <Signup />
			}
		]
	}
]);

ReactDOM.createRoot(document.getElementById("root")!).render(
	<React.StrictMode>
		<ChakraProvider
			theme={extendTheme({
				config: {
					initialColorMode: "dark",
					useSystemColorMode: false
				}
			})}
		>
			<Suspense fallback={<p>Loading...</p>}>
				<RouterProvider router={router} />
			</Suspense>
		</ChakraProvider>
	</React.StrictMode>
);
